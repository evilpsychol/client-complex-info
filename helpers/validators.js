export const password = v => (v && v.length >= 8) || 'Минимум 8 символов'
export const required = v => !!v || 'Это обязательное поле'
