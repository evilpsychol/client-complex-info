import { isObject } from 'vue-js-modal/src/utils/types'

export const state = () => ({
  list: [],
  pagination: null,
  sort: null,
  filterInfo: null,
  loadingList: false,
  filter: {
    queue_id: null,
    building_id: null,
    room_type: null,
    ending: null,
    window: null,
    price: {
      min: null,
      max: null
    },
    floor: {
      min: null,
      max: null
    },
    square: {
      min: null,
      max: null
    },
    kitchen: {
      min: null,
      max: null
    },
    top: null,
    balcony: null,
    status: null,
    facing: null
  }
})

export const getters = {
  filter: (state) => {
    const res = {}
    for (const k in state.filter) {
      let v = state.filter[k]
      if (v === true) {
        v = 1
      }
      if (v && isObject(v)) {
        for (const kp in v) {
          res[`${k}_${kp}`] = v[kp]
        }
      } else {
        res[k] = v
      }
    }
    return res
  }
}

export const mutations = {
  CLEAR_FLATS (state) {
    state.list = []
    state.pagination = null
  },
  SET_LIST (state, data) {
    state.list = state.list.concat(data)
  },
  SET_LOADING_LIST (state, data) {
    state.loadingList = data
  },
  SET_SORT (state, data) {
    state.sort = data
  },
  SET_FILTER_INFO (state, data) {
    state.filterInfo = data
  },
  CHANGE_FILTER (state, { attr, val }) {
    if (~attr.indexOf('.')) {
      const [name, type] = attr.split('.')
      state.filter[name][type] = val
    } else {
      state.filter[attr] = val
    }
  },
  SET_PAGINATION (state, data) {
    state.pagination = {
      page: Number(data['x-pagination-current-page']),
      pages: Number(data['x-pagination-page-count']),
      rowsPerPage: Number(data['x-pagination-per-page']),
      totalItems: Number(data['x-pagination-total-count'])
    }
  }
}

export const actions = {
  async loadList ({ state, getters, rootState, commit }) {
    commit('SET_LOADING_LIST', true)
    try {
      const params = {
        ...getters.filter,
        expand: 'buildingInfo.queue,room,apartments.facingInfo,apartments.direction',
        complex_id: rootState.complex.select.id,
        sort: state.sort,
        'per-page': 16
      }
      if (state.pagination && state.pagination.pages > state.pagination.page) {
        params.page = state.pagination.page + 1
      }
      if (!state.pagination || state.pagination.pages !== state.pagination.page) {
        const { data, headers } = await this.$api.complex.flats.list(params)
        commit('SET_LIST', data)
        commit('SET_PAGINATION', headers)
      }
    } catch (e) {
      this.$response.errors(e)
    }
    commit('SET_LOADING_LIST', false)
  },
  async changeFilter ({ commit, dispatch }, { attr, val }) {
    commit('CHANGE_FILTER', { attr, val })
    commit('CLEAR_FLATS')
    await dispatch('loadList')
    await dispatch('loadFilterInfo')
  },
  async changeSort ({ commit, dispatch }, data) {
    commit('SET_SORT', data)
    commit('CLEAR_FLATS')
    await dispatch('loadList')
  },
  async loadFilterInfo ({ getters, rootState, commit }) {
    try {
      const data = await this.$api.complex.flats.filter(rootState.complex.select.id, getters.filter)
      commit('SET_FILTER_INFO', data)
    } catch {}
  }
}
