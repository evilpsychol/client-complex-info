export const state = () => ({
  list: [],
  select: null,
  progress: null,
  queues: null
})

export const getters = {}

export const mutations = {
  SET_LIST (state, data) {
    state.list = data
  },
  SET_COMPLEX (state, data) {
    state.select = data
    state.progress = null
    state.queues = null
  },
  CLEAR_COMPLEX (state) {
    state.select = null
    state.progress = null
    state.queues = null
  },
  SET_PROGRESS (state, data) {
    state.progress = data
  },
  SET_QUEUES (state, data) {
    state.queues = data
  },
  ADD_PROGRESS (state, data) {
    state.progress.push(data)
  },
  SET_PROGRESS_IMAGES (state, { progressId, data }) {
    const find = state.progress.find(e => e.id === progressId)
    if (find) {
      find.images = find.images ? find.images.concat(data) : data
    }
  },
  DELETE_PROGRESS_IMAGE (state, imageId) {
    for (let i = 0; i < state.progress.length; i++) {
      const find = state.progress[i].images.findIndex(e => e.id === imageId)
      if (~find) {
        state.progress[i].images.splice(find, 1)
        break
      }
    }
  }
}

export const actions = {
  async loadList ({ commit }) {
    try {
      const data = await this.$api.complex.list()
      commit('SET_LIST', data)
    } catch {}
  },
  async loadComplex ({ commit }, complexId) {
    try {
      commit('CLEAR_COMPLEX', null)
      commit('flats/CLEAR_FLATS', null, { root: true })
      const data = await this.$api.complex.one(complexId, { expand: 'images' })
      commit('SET_COMPLEX', data)
    } catch {}
  },
  async loadQueues ({ state, commit }) {
    try {
      const data = await this.$api.complex.queues.list(state.select.id, { expand: 'buildings.params,buildings.position,image,buildings.stat.room' })
      commit('SET_QUEUES', data)
    } catch {}
  },
  async loadProgress ({ state, commit }) {
    try {
      const data = await this.$api.complex.progress.list(state.select.id, { expand: 'images' })
      commit('SET_PROGRESS', data)
    } catch {}
  },
  async addProgress ({ state, commit }, { complexId, date }) {
    try {
      const data = await this.$api.complex.progress.change({
        complex_id: complexId,
        date
      })
      commit('ADD_PROGRESS', data)
    } catch {}
  },
  async loadProgressImages ({ state, commit }, { progressId, images }) {
    try {
      const data = await this.$api.complex.progress.images(progressId, images)
      commit('SET_PROGRESS_IMAGES', { progressId, data })
    } catch {}
  },
  async deleteProgressImage ({ commit }, imageId) {
    try {
      await this.$api.complex.progress.deleteImage(imageId)
      this.$noty.success('Изображение удалено')
      commit('DELETE_PROGRESS_IMAGE', imageId)
    } catch {}
  }
}
