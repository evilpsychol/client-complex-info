export const state = () => ({
  complexList: []
})

export const getters = {}

export const mutations = {
  SET_COMPLEX_LIST (state, data) {
    state.complexList = data
  }
}

export const actions = {
  async loadComplexList ({ commit }) {
    try {
      const data = await this.$api.esbn.complex.list({ fields: 'id,title' })
      commit('SET_COMPLEX_LIST', data)
    } catch { }
  }
}
