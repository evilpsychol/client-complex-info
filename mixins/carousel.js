export const carouselMain = {
  data () {
    return {
      slide: null,
      imagePath: null
    }
  },
  methods: {
    selectImage (path) {
      this.imagePath = path
    }
  }
}
