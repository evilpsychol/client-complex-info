import moment from 'moment'

export default (ctx, inject) => {
  moment.locale('ru')
  inject('moment', moment)
}
