import Vue from 'vue'
import Notifications from 'vue-notification'

Vue.use(Notifications)

export default (app, inject) => {
  inject('noty', {
    error (text, title = '') {
      Vue.notify({
        title,
        text,
        type: 'error'
      })
    },
    success (text, title = '') {
      Vue.notify({
        title,
        text,
        type: 'success'
      })
    }
  })
}
