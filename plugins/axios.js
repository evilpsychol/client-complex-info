export default function ({ $axios, app }) {
  $axios.onResponseError((error) => {
    const r = error.response
    if (!r) {
      const c = error.config
      app.$noty.error(`${c.method.toUpperCase()}: ${c.baseURL}${c.url}`, error.message)
    }
    if (!Array.isArray(r.data)) {
      app.$noty.error(r.data.message)
    }
  })
}
