import createApi from '~/api'
export default (ctx, inject) => {
  const repositoryWithAxios = createApi(ctx.$axios)
  inject('api', repositoryWithAxios)
}
