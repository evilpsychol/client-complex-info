import format from '~/helpers/format'

export default (ctx, inject) => {
  inject('helpers', {
    format
  })
}
