export default ({ app }, inject) => {
  inject('response', {
    errors (e, fields = []) {
      const r = e.response
      const res = {}
      if (r && r.status === 422) {
        r.data.forEach((item) => {
          if (fields.includes(item.field)) {
            res[item.field] = item.message
          } else {
            app.$noty.error(item.message)
          }
        })
      } else {
        app.$noty.error(e.message)
      }
      return res
    }
  })
}
