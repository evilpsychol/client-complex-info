export default $axios => ({
  list: params => $axios.$get('complex/list', { params }),
  one: (id, params) => $axios.$get(`complex/${id}`, { params }),
  add: esbnId => $axios.$post('complex/add', { esbn_id: esbnId }),
  queues: {
    list: (id, params) => $axios.$get(`complex/${id}/queues/list`, { params }),
    update: (id, params) => $axios.$post(`complex/queues/${id}`, params),
    delete: id => $axios.$delete(`complex/queues/${id}`),
    add: params => $axios.$post('complex/queues/add', params),
    image: (id, image) => {
      const formData = new FormData()
      formData.append('image', image)
      return $axios.$post(`complex/queues/${id}/image`, formData)
    },
    deleteImage: id => $axios.$delete(`complex/queues/${id}/image`),
    changeBuilding: params => $axios.$post('complex/queues/change-building', params)
  },
  buildings: {
    change: (id, params) => $axios.$post(`complex/buildings/${id}`, params),
    position: (id, params) => $axios.$post(`complex/buildings/${id}/position`, params)
  },
  progress: {
    change: params => $axios.$post('complex/progress/change?expand=images', params),
    list: (id, params) => $axios.$get(`complex/${id}/progress`, { params }),
    delete: id => $axios.$delete(`complex/progress/${id}`),
    images: (id, images) => {
      const formData = new FormData()
      images.forEach((e) => {
        formData.append('images[]', e)
      })
      return $axios.$post(`complex/progress/${id}/image`, formData)
    },
    deleteImage: id => $axios.$delete(`complex/progress/image/${id}`)
  },
  flats: {
    list: params => $axios.get('complex/flats', { params }),
    filter: (id, params) => $axios.$get(`complex/${id}/flats/filter`, { params })
  }
})
