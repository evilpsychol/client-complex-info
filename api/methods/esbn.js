export default $axios => ({
  complex: {
    list: params => $axios.$get('esbn/complex/list', { params })
  }
})
