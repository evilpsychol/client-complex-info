export default $axios => ({
  self: () => $axios.$get('users/self'),
  login: params => $axios.$post('users/login', params)
})
