import user from '@/api/methods/user'
import esbn from '@/api/methods/esbn'
import complex from '@/api/methods/complex'

export default $axios => ({
  user: user($axios),
  esbn: esbn($axios),
  complex: complex($axios),
  constants: expand => $axios.$get('constants', { params: { expand } })
})
